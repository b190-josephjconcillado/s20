console.log("Hello World!");

// While loop
// takes in an expression/condition; expressions are any unit of code that can be evaluated to a value; iteration is the term given to the repetition of the satements
// syntax: while(expression/condition){
//     statements
// }
// let count = 5;
// while(count !== 0){
//     console.log("While loop result: "+count);
//     count --;
// };
// console.log("-------");
// count = 0;
// while(count <= 10){
//     console.log("While loop result: "+count);
//     count ++;
// };

// Do-while loop
/*
    1. the statement in do block executes once
    2. the message in the statements will be executed
    3. after eecuting once, the while statement will evaluate wheter to run the uteration of the loop or not
    based on the expression
    4. if the is true, an itereation will be done
    5. if the ondition is false, the loop will stop
*/
// let number = Number(prompt("Give me a number"));

// do{
//     console.log("Do-while Loop: "+number);
//     number++;
//     // number+=1; the same with number++
// }while(number < 10)

// for loop sir, pwde po ba magloop inside a loop?
/*
    the most flexible looping compared to do-while and while loops. it has 3 parts. 
    1. initialization - tracks the progression of the loop
    2. condition/expression - will be evaluated will determine whether the loop will run one more time.
    3. final expression - indicates how to advance the loop

    stages of for loop based on the code below
        - will initialize a variable "count" that has the value of 0;
        - the condition/expression that is to be assessed is if the value of "count" is less than or equal to 20
        -perform the statement should the condition/expression returns true
        -increment the value of count
*/

// for(let count = 1; count <=20; count ++){
//     console.log("For loop: "+count);
// }

// let myString = "alex";
// console.log(myString);

// for(let x = 0; x < myString.length; x++){
//     console.log(myString[x]);
// };

// let myName = "Joseph";
// myName = myName.toLowerCase();

// for(let x = 0; x < myName.length; x++){
//     if(myName[x] === "a" || myName[x] === "e" ||  myName[x] === "i" || myName[x] === "o" || myName[x] === "u" ){
//         console.log(3);
//     }
//     else{
//         console.log(myName[x]);
//     }

// };

// Continue and break statements
/*
for(let count = 0; count <= 20; count++){
    if(count % 2 === 0){
        continue;
    }
    console.log("Continue and Break: "+count);

    if(count>10){
        break;
    };
};
*/

let name = "alexandro";

for(let x = 0; x < name.length; x++){
    console.log(name[x]);
    if(name[x] == "a"){
        continue;
    }else if(name[x] == "d"){
        break;
    }
};
for(let x = 0; x <= 3; x++){
    for(let y = 0; y <= x; y++){
        console.log("x: " + x + " y: " + y);
    }
}
