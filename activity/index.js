console.log("Hello World!");

let number = Number(prompt("Enter a number: "));
console.log("The number you provided is "+number+".");
for(number; number > -1; number--){
    if(number <= 50){
        console.log("The current value is at 50. Terminating the loop.");
        break;
    };
    if(number%10 === 0){
        console.log("The number is divisible by 10. Skipping the number.");
        continue;
    };
    if(number%5 === 0){
        console.log(number);
    };
};

let string = "supercalifragilisticexpialidocious";
console.log(string);
let consonant = "";
for(x = 0; x < string.length; x++){
    if(string[x] === "a" || string[x] === "e" || string[x] === "i" || string[x] === "o" || string[x] === "u" ){
        continue;
    }else{
        consonant = consonant + string[x];
    }
};
console.log(consonant);